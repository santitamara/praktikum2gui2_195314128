/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3B;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import static java.nio.file.Files.list;
import static java.rmi.Naming.list;
import static java.util.Collections.list;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author ASUS
 */
public class lat5 extends JFrame{
      private static final int FRAME_WIDTH = 300;
     private static final int FRAME_HEIGHT = 200;
      private static final int FRAME_X_ORIGIN = 150;
       private static final int FRAME_Y_ORIGIN  = 300;
       
       public static void main(String[] args) {
         lat5 frame = new lat5();
        frame.setVisible(true);
}
    public lat5(){
    Container contentPane;
    JPanel listPanel, okPanel;
    
    JButton okButton;
    String[]names = {"Ape","Bat","Bee","Cat",
                     "Dog","Eel","Fox","Gnu",
                     "Han","Man","Sow","Yak"};
    
            setSize(FRAME_WIDTH,FRAME_HEIGHT);
            setTitle("Program Ch14FlowLayoutSample2");
            setLocation(FRAME_X_ORIGIN,FRAME_Y_ORIGIN);
            
            contentPane = getContentPane();
            contentPane.setBackground(Color.white);
            contentPane.setLayout(new BorderLayout());
            
            listPanel = new JPanel(new GridLayout(0,1));
            listPanel.setBorder(BorderFactory.createTitledBorder("Three-letter Animal Names"));
            
          JList list = new JList(names);
            listPanel.add(new JScrollPane(list));
            
            okPanel = new JPanel(new FlowLayout());
            okButton = new JButton("OK");
            okPanel.add(okButton);
            
            contentPane.add(listPanel,BorderLayout.CENTER);
            contentPane.add(okPanel, BorderLayout.SOUTH);
             
             setDefaultCloseOperation(EXIT_ON_CLOSE);
                       
}
}
