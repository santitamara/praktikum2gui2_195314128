/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3C;

/**
 *
 * @author ASUS
 */
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Asus
 */
public class nomor1 extends JFrame {

    public static void main(String[] args) {  //fungsi utama
        nomor1 frame = new nomor1();
        frame.setVisible(true);
    }

    public nomor1() {
        Container contentPane;
        JButton button1, button2;

        contentPane = getContentPane();
        contentPane.setBackground(Color.blue);

        contentPane.setLayout(new FlowLayout());

        button1 = new JButton("Tampil 1");
        button2 = new JButton("Tampil 2");
        contentPane.add(button1);
        contentPane.add(button2);

        JMenuBar menuBar;
        JMenu file, edit;
        setTitle("Frame Pertama");
        setSize(300, 300);
        file = new JMenu("File");
        edit = new JMenu("Edit");

        menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        menuBar.add(file);
        menuBar.add(edit);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
}
