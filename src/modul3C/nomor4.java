/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3C;

/**
 *
 * @author ASUS
 */
import java.awt.*;
import javax.swing.*;

public class nomor4 extends JDialog {

    private JButton button_Left, button_Right;
    private JCheckBox centered, bold, italic;
    private JTextArea txt;

    public nomor4() {
        this.setLayout(null);

        button_Left = new JButton("Left");
        button_Left.setBounds(130, 90, 80, 20);
        this.add(button_Left);

        button_Right = new JButton("Right");
        button_Right.setBounds(40, 90, 80, 20);
        this.add(button_Right);

        txt = new JTextArea("Welcome to Java");
        txt.setBounds(1, 1, 230, 85);
        this.add(txt);

        centered = new JCheckBox("Centered");
        centered.setBounds(230, 1, 100, 30);
        this.add(centered);

        bold = new JCheckBox("Bold");
        bold.setBounds(230, 30, 100, 30);
        this.add(bold);

        italic = new JCheckBox("Italic");
        italic.setBounds(230, 60, 100, 30);
        this.add(italic);

    }

    public static void main(String[] args) {
        nomor4 scan = new nomor4();
        scan.setSize(320, 145);
        scan.setVisible(true);
        scan.setResizable(false);
        scan.setTitle("CheckBoxDemo");
        scan.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    }

}
