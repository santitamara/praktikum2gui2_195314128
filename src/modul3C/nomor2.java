/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3C;

/**
 *
 * @author ASUS
 */
import java.awt.Button;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JDialog;

public class nomor2 extends JDialog {

    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 600;

    public static void main(String[] args) {
        nomor2 gui = new nomor2();
        gui.setVisible(true);
    }

    public nomor2() {
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        Container contentPane;
        JButton YELLOW, BLUE, RED;
        setLayout(null);
        contentPane = getContentPane();

        YELLOW = new JButton("YELLOW");
        YELLOW.setBounds(25, 100, 100, 50);
        contentPane.add(YELLOW);
        
        BLUE = new JButton("BLUE");
        BLUE.setBounds(175, 100, 100, 50);
        contentPane.add(BLUE);
        
        RED = new JButton("Red");
        RED.setBounds(325, 100, 100, 50);
        contentPane.add(RED);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
}
