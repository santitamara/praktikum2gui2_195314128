/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3C;

import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IMAM
 */
public class nnomer7 extends JDialog{
    private JList listNegara;
    private JLabel label_indonesia,label_australia,label_denmark,label_portugal,label_jamaica,label_malaysia,label_brazil,label_canada,label_usa;
    
    String[] negara= {"Indonesia","Australia","Denmark","Portugal","Jamaica","Malaysia","Brazil","Canada","USA"};
    
    public nnomer7(){
        setTitle("ListDemo");
        setSize(1000,500);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
        iniLabelGambar();
        iniListNegara();
        
    }
    public void iniListNegara(){
        listNegara = new JList(negara);
        listNegara.setBounds(1, 1, 200, 100);
        JScrollPane sc = new JScrollPane(listNegara,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        sc.setBounds(1, 1, 200, 170);
        this.add(sc);
    }
    public void iniLabelGambar(){
        setLayout(null);
       URL bendera = this.getClass().getResource("indonesia.png");
       URL bendera1 = this.getClass().getResource("australia.png");
       URL bendera2 = this.getClass().getResource("denmark.png");
       URL bendera3 = this.getClass().getResource("portugal.png");
       URL bendera4 = this.getClass().getResource("jamaica.png");
       URL bendera5 = this.getClass().getResource("brazil.png");
       URL bendera6 = this.getClass().getResource("canada.png");
       URL bendera7 = this.getClass().getResource("usa.png");
       
       ImageIcon image = new ImageIcon(bendera);
       ImageIcon image1 = new ImageIcon(bendera1);
       ImageIcon image2= new ImageIcon(bendera2);
       ImageIcon image3 = new ImageIcon(bendera3);
       ImageIcon image4 = new ImageIcon(bendera4);
       ImageIcon image5 = new ImageIcon(bendera5);
       ImageIcon image6 = new ImageIcon(bendera6);
       ImageIcon image7 = new ImageIcon(bendera7);
       
       label_indonesia = new JLabel(image);
       label_indonesia.setBounds(200, 110, 150,100);
       this.add(label_indonesia);
       label_australia = new JLabel(image1);
       label_australia.setBounds(200, 110, 150, 100);
       this.add(label_australia);
       label_denmark = new JLabel(image2);
       label_denmark.setBounds(200, 110, 150, 100);
       this.add(label_denmark);
       label_portugal = new JLabel(image3);
       label_portugal.setBounds(200, 110, 150, 100);
       this.add(label_portugal);
       label_jamaica = new JLabel(image4);
       label_jamaica.setBounds(200, 110, 150, 100);
       this.add(label_jamaica);
       label_brazil = new JLabel(image5);
       label_brazil.setBounds(200, 110, 150, 100);
       this.add(label_brazil);
       label_canada = new JLabel(image6);
       label_canada.setBounds(200, 110, 150, 100);
       this.add(label_canada);
       label_usa = new JLabel(image7);
       label_usa.setBounds(200, 110, 150, 100);
       this.add(label_usa);
       
       
    }
    public static void main(String[] args) {
        new nnomer7();
    }
}